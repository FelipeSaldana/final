Rails.application.routes.draw do
  devise_for :admins, controllers:{
      sessions: 'admins/sessions'
      }
  devise_for :users, controllers:{
        sessions: 'users/sessions'
      }
    
    devise_scope :admin do
       get "admins/users/lista",
       to: "admins/sessions#lista",
       as: "users_list"    
    end    
  resources :nuevos
  
  resources :zombies do
      resources :brains
  end      
    
    root to: 'zombies#index'
  #get "sexto_repo/cerebros", to: 'brains#index', as: 'brains'    
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
